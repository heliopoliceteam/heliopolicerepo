/*jslint browser: true*/
/*global $, jQuery, alert*/
$(function(){
    'use strict';
    $('.banner').bxSlider({
        auto: true,
        pager: false,
        touchEnabled: false,
        onSliderLoad: function () {
            $('.banner>li .cap_block').eq(1).addClass('active-slide');
            $(".cap_block.active-slide").addClass("wow animated fadeInUp");
        },
        onSlideAfter: function (currentSlideNumber, totalSlideQty, currentSlideHtmlObject) {
            //console.log(currentSlideHtmlObject);
            $('.active-slide').removeClass('active-slide');
            $('.banner>li .cap_block').eq(currentSlideHtmlObject + 1).addClass('active-slide');
            $(".cap_block.active-slide").addClass("wow animated fadeInUp");

        },
        onSlideBefore: function () {
            $(".cap_block.active-slide").removeClass("wow animated fadeInUp");
            $(".one.cap_block.active-slide").removeAttr('style');
        },
        nextSelector: '#banner_next',
        prevSelector: '#banner_prev',
        nextText: '<i class="fa fa-angle-right"></i>',
        prevText: '<i class="fa fa-angle-left"></i>',
    });
    // Navigation
    $('.nav_trigger').click(function(){
        $('.navigation').slideToggle();
    });
    $(window).resize(function(){
        if($(window).width() >= 768){
            $('.navigation').show();
        } else {
            $('.navigation').hide();
        }
    });
});