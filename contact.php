<?php
//session_start();
$mes = $_REQUEST['mes'];
?>
<!DOCTYPE HTML>
<html lang="">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="images/favicon.png">
    <title>Helio Police</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
    <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    
    
    <script src="js/jquery-2.1.4.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.bxslider.js"></script>
    <script src="js/script.js"></script>
    <script src="http://maps.googleapis.com/maps/api/js"></script>
    <script>
        var myCenter = new google.maps.LatLng(11.5938551, 75.7744412);

        function initialize() {
            var mapProp = {
                center: myCenter,
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var map = new google.maps.Map(document.getElementById("gmap"), mapProp);

            var marker = new google.maps.Marker({
                position: myCenter,
            });

            marker.setMap(map);
        }

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    
</head>
    
<body>
    <!-- Header -->
    <div class="header">
        <div class="container">
            <div class="logo">
                <a href="index.html"><img src="images/logo.png" alt="Helio Police" /></a>
            </div>
            <div class="nav_wrap">
                <ul class="navigation">
                    <li><a href="index.html">home</a></li>
                    <li><a href="aboutus.html">about</a></li>
                    <li><a href="products.html">products</a></li>
                    <!--<li><a href="#">services</a></li>-->
                    <li><a href="contact.php" class="active">contact</a></li>
                </ul>
            </div>
            <div class="bd_clear"></div>
        </div>
        <span class="nav_trigger"><i class="fa fa-bars"></i></span>
    </div>
    <!-- /Header -->
    
    <div class="contact_wrap">
        <div class="contact_form_wrap">
            <div class="contact_details">
                <h3>Contact</h3>
                
                <div class="contact_det_block">
                    <span class="contact_span">                        
                        <i class="fa fa-envelope contact_ico"></i>                            
                        <a href="mailto:customercare@heliopolice.com">customercare@heliopolice.com</a>                            
                    </span>
                    <span class="contact_span">
                        <i class="fa fa-mobile contact_ico"></i>
                        <a href="tel:+918606655050">+91 8606655050</a>
                    </span>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
					<?php
						if($mes){
						?>
						<p class="text-left" style="color: #FFF; margin: 15px 0px;">
							<?= $mes; ?>
						</p>
						<?php
						}
						
							
					?>
                    <form class="contact_form" method="post" action="mail.php">
                        <div class="form_block">
                            <input type="text" name="name" required placeholder="NAME" />
                        </div>
                        <div class="form_block">
                            <input type="email" name="email" required placeholder="EMAIL" />
                        </div>
                        <div class="form_block">
                            <textarea name="message" required placeholder="MESSAGE"></textarea>
                        </div>
                        <div class="form_block_submit">
                            <input type="submit" name="submit" value="SEND" />
                        </div>
                    </form>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div id="gmap">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Footer -->
    <div class="footer">
        <div class="footer_inner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="footer_logo">
                            <a href="index.html"><img src="images/footer_logo.png" alt="" /></a>
                        </div>
                    </div>                     
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="add_block">
                            <p>Helio Police<br /></p>
                            <p>Email: <a href="mailto: customercare@heliopolice.com">customercare@heliopolice.com</a></p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="footer_nav">
                            <div class="footer_nav_inner">
                                <div class="soc_wrap">
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-google-plus"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                </div>
                                <div class="footer_navigation">
                                    <ul>
                                        <li><a href="index.html">home</a></li>
                                        <li><a href="aboutus.html">about</a></li>
                                        <li><a href="products.html">products</a></li>
                                        <!--<li><a href="#">services</a></li>-->
                                        <li><a href="contact.php">contact</a></li>
                                    </ul>
                                </div>
                            </div>                            
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="copy_wrap">
                            <span class="copy_rt">Copyright &copy; 2016 Helio Police. All Rights Reserved.</span>
                            <span class="power">Powered by <a href="http://www.bodhiinfo.com" target="_blank"><img src="images/bodhi_logo.png" alt="" /></a></span>
                            <div class="bd_clear"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
